import numpy as np
import pygame as pg


class WARP:
    __upper_limit = 10
    __lower_limit = 0
    __x_speed = 0
    __y_speed = 0
    __acceleration = 0.05
    
    @classmethod
    def __update(cls):
        # begin in the lower limit
        # approach asymptotically to the upper limit
        
        # 1st approach, cyclometric asymptote
        cls.__y_speed = np.arctan(cls.__acceleration * cls.__x_speed) * cls.__upper_limit * 2 / np.pi + cls.__lower_limit
        
        # 2nd approach, hyperbolic asymptote
        #cls.__y_speed = cls.__upper_limit * (1 - 1 / (cls.__acceleration * cls.__x_speed + 1)) + cls.__lower_limit

    @classmethod
    def increment(cls):
        cls.__x_speed += 1
        cls.__update()
    
    @classmethod
    def decrement(cls):
        cls.__x_speed = max(cls.__x_speed - 1, 0)
        cls.__update()
            
    @classmethod
    def get_speed(cls):
        return cls.__y_speed


# example of use    
def stop_warp():
    while WARP.get_speed() > 0:
        WARP.decrement()
        print(WARP.get_speed())


# presentation
for i in range(100):
    print(WARP.get_speed())
    WARP.increment()
    
print('\n\nstart decrematation\n')
stop_warp()