import numpy as np
import os
import pygame
from warp_speed_manipulations import *


WINDOW_SIZE = (800, 600)
BACKGROUND_SIZE = (3880, 600)
WINDOW_POSITION = (100, 100)
FLAGS = 5

FONT_SIZE = 24
FONT_BOLD = False

#### setup & initialization ####

# window position
os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % WINDOW_POSITION

# start pygame
pygame.init()

# setting window size
screen = pygame.display.set_mode(WINDOW_SIZE, FLAGS)

pygame.display.set_caption(os.path.basename(__file__) + " in " + os.getcwd() )

# getting default font
font = pygame.font.SysFont(pygame.font.get_default_font(),FONT_SIZE)
font.set_bold(FONT_BOLD)

clock = pygame.time.Clock()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/

# Loads and scales images
src1 = pygame.image.load('1.png')
src2 = pygame.image.load('2.png')
src3 = pygame.image.load('3.png')
nebula = pygame.image.load('mg.png')
nebula = pygame.transform.rotozoom(nebula, 0, 0.2)
starship = pygame.image.load('Ship.png')
starship = pygame.transform.rotozoom(starship, 0, 0.5)

# Preparing speed scale
warp_speed = [0, 1, 10, 39, 102, 214, 392, 656, 1024, 1516]
warp_fraction = 0
speed = warp_speed[warp_fraction]

# Differences between speed of star layers
delta_1 = 1
delta_2 = 1.5
delta_3 = 2

# Layers speed
vel1 = 0
vel2 = 0
vel3 = 0

# Vertical move of ship
vertical = 0

delta_vert = 10

while True:
    for event in pygame.event.get():
        #        print(event)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_d:
                warp_fraction += 1
                if warp_fraction > 9:
                    warp_fraction = 9
                speed = warp_speed[warp_fraction]
            if event.key == pygame.K_a:
                warp_fraction -= 1
                if warp_fraction < 0:
                    warp_fraction = 0
                speed = warp_speed[warp_fraction]
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

    screen.fill(000000)
    screen.blit(nebula, (WINDOW_SIZE[0] * 0.618, WINDOW_SIZE[1] * 0.618))

    # first layer
    vel1 -= delta_1*speed
    screen.blit(src1, (vel1, 0))
    screen.blit(src1, (BACKGROUND_SIZE[0]+vel1, 0))
    if vel1 <= -BACKGROUND_SIZE[0] - speed*delta_1:
        screen.blit(src1, (BACKGROUND_SIZE[0]+vel1, 0))
        vel1 = 0

    # second layer
    vel2 -= delta_2*speed
    screen.blit(src2, (vel2, 0))
    screen.blit(src2, (BACKGROUND_SIZE[0]+vel2, 0))
    if vel2 <= -BACKGROUND_SIZE[0] - speed*delta_2:
        screen.blit(src2, (BACKGROUND_SIZE[0]+vel2, 0))
        vel2 = 0

    # third layer
    vel3 -= delta_3*speed
    screen.blit(src3, (vel3, 0))
    screen.blit(src3, (BACKGROUND_SIZE[0]+vel3, 0))
    if vel3 <= -BACKGROUND_SIZE[0] - speed*delta_3:
        screen.blit(src3, (BACKGROUND_SIZE[0]+vel3, 0))
        vel3 = 0

    # check if starship is moving vertically
    keys = pygame.key.get_pressed()
    if keys[pygame.K_w]:
        vertical -= delta_vert
        if vertical < -starship.get_height():
            vertical = -starship.get_height()
    if keys[pygame.K_s]:
        vertical += delta_vert
        if vertical > starship.get_height():
            vertical = starship.get_height()
    # starship
    screen.blit(starship, (0, vertical + ((WINDOW_SIZE[1] - starship.get_height())/2)))

    pygame.display.update()
    clock.tick(30)
